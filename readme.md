# 妙娃
## 简介
`miaowa`是一款专注于速卖通运营的工具机器人。将一些运营店铺时发现的重复有规范步骤的事情，通过程序的自动化处理来提升运营效率。

文档请跳转：https://mmtdd.gitee.io/miaowa/

## 软件环境
```shell
pip install -r requirements.txt
```
* python==3.6
* selenium==3.141.0

[ChromeDriver](https://googlechromelabs.github.io/chrome-for-testing/#/stable)

## 对象设计
### 店 Store
1. 维护店相关的地址和状态

### 机器人 Robot
1. 创建浏览器
2. 创建店
3. 登录店
4. 获取订单列表
5. 处理follow
6. 处理review
7. 关机

### 浏览器 Browser
1. 创建谷歌浏览器

### 配置类
1. 维护评论配置
2. 维护follow消息配置
3. 下一步重试次数

### 命令行
基于argparse模块集成

### 日志工具
实现warning,debug,info,error4个级别的日志

## 熟练Python知识点
* 面向对象编程 class,@staticmethod
* 对象的访问控制，如__a私有变量，a公共变量
* argparse模块集成
* 浏览器selenium模块自动化任务实现

## 打包发布
先安装`pyinstaller`
```
pip install pyinstaller
```
然后执行打包命令：
```
pyinstaller --onefile --windowed app.py
```
在dist目录下得到对应操作系统上的可执行版本


## 更新日志
### 2023-03-xx
1. 将程序修改为子命令，`raf`命令做`review`和`follow`任务。
``` shell
conda activate miaowa
python app.py -sk=toy -bh=close raf -p=3
```