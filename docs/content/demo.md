目前包括如下示例，您可以用来用自己的店铺体验这些示例。

## [处理店铺的follow和review](content/demo-follow-review.md)
这个示例针对[订单评价管理页](https://feedback.aliexpress.com/management/feedbackSellerList.htm)中>=4星的订单，给对应买家发送follow消息和给出友好评价。

