关于如何安装 `miaowa` 的说明。

## 系统需求
由于采用 `Python3` 编写，需要安装`Python>=3.6`的环境。

## 下载
目前可以在 Gitee 的 [Release](https://gitee.com/mmtdd/miaowa/releases/tag/v1.0.0) 页面中下载到`V1.0.0`版本源码。

或者直接下载最新源码：
``` shell
git clone https://gitee.com/mmtdd/miaowa.git
```

## 安装
1. 基于[conda](https://docs.conda.io/en/latest/miniconda.html)创建虚拟环境
``` shell
conda create -n miaowa python=3.6
```
2. 切换并激活`miaowa`虚拟环境
``` shell
conda activate miaowa
```
3. 安装依赖
``` shell
pip3 install -r requirements.txt
```
4. 运行测试显示usage
``` shell
python3 app.py --help
```
出现如下信息，说明运行成功。

    ``` shell
    (miaowa) mango@mangodeMacBook-Pro miaowa % python app.py --help
    usage: python app.py -sk=sport -bh=True

    欢迎使用妙娃机器人v1.0.0

    optional arguments:
    -h, --help            show this help message and exit
    -v, --version         查看版本号
    -sk , --store_kind    店类型，如sport-运动；toy-玩具；默认sport
    -bt , --browser_type 
                            浏览器类型，如chrome-谷歌；firefox-火狐；默认chrome
    -bh , --browser_headless 
                            无头浏览器运行，open-打开；close-关闭；默认open
    -wf , --work_follow   运行follow任务，open-打开；close-关闭；默认open
    -wr , --work_review   运行review任务，open-打开；close-关闭；默认open
    -ll , --log_level     运行review任务，error-错误；info-提醒；debug-调试；warning-警告；默认info
    -of , --order_from    订单号来源，json-从json文件中加载；spider-爬虫自动获取；默认spider
    -p , --page           当前页，默认1
    ```

## 下一步？
* [示例](content/demo.md)：通过简单示例了解 `miaowa` 的应用场景。
