# coding=utf-8
import argparse
from compontent.cmd.CmdManager import CmdManager

# 版本号
def get_version():
    return "1.0.0"

# 命令行参数类
class Argument:
    # 参数
    __args = None

    # 初始化主参数
    def init_main_args(self,parser):
        parser.add_argument('-v', '--version',
                            action="version",
                            version=get_version(),
                            help="查看版本号")
        parser.add_argument('-sk', '--store_kind',
                            default="sport",
                            type=str, metavar="",
                            required=False,
                            help="店类型，如sport-运动；toy-玩具；默认%(default)s")
        parser.add_argument('-bt', '--browser_type',
                            default="chrome",
                            type=str, metavar="",
                            required=False,
                            help="浏览器类型，如chrome-谷歌；firefox-火狐；默认%(default)s")
        parser.add_argument('-bh', '--browser_headless',
                            default="open",
                            type=str,
                            metavar="",
                            required=False,
                            help="无头浏览器运行，open-打开；close-关闭；默认%(default)s")
        parser.add_argument('-ll', '--log_level',
                            default="debug",
                            type=str,
                            metavar="",
                            required=False,
                            help="日志级别，error-错误；info-提醒；debug-调试；warning-警告；默认%(default)s")
        
        pass

    def __init__(self):
        description = "欢迎使用妙娃机器人v" + get_version()
        usage = "python app.py -sk=sport -bh=True"
        parser = argparse.ArgumentParser(usage=usage,
                                         prog="miaowa",
                                         description=description,
                                         allow_abbrev=False)

         # 初始化参数
        self.init_main_args(parser)
        # 注册子命令
        CmdManager.register(parser)
        self.__args = parser.parse_args()
        pass

    # 获取参数
    def get_args(self):
        return self.__args

    def check_browser_headless(self):
        return self.__args.browser_headless == 'open'
