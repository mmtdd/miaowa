# coding=utf-8
from compontent.cmd.ReviewAndFollowCmdExecutor import ReviewAndFollowCmdExecutor
from compontent.cmd.StoreDataCmdExecutor import StoreDataCmdExecutor

# cmd管理器
class CmdManager:
    _raf = ReviewAndFollowCmdExecutor()
    _sd = StoreDataCmdExecutor()
    # 类型实现
    __impl = {
        "raf": _raf,
        "sd": _sd
    }

    @staticmethod
    def get_instance(impl):
        return CmdManager.__impl.get(impl)

    # 注册命令
    @staticmethod
    def register(parser):
        subparsers = parser.add_subparsers(help='子命令说明')
        for executor in CmdManager.__impl.values():
            executor.register(subparsers)

    
if __name__ == '__main__':
    CmdManager.register()

